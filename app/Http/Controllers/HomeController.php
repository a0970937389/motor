<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Models\History;
use App\Models\WrongTime;
use Exception;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user = Auth::user();
        return view('index', ['user'=>$user]);
    }

    public function w(){
        return view('w');
    }

    public function history_add(Request $request){
        $history = History::create($request->all());
        if(!is_null($history))
            return 'success';
        else
            return 'fail';
    }


    public function wrongtime_add(Request $request){
        $wrongtime = WrongTime::create($request->all());
        if(!is_null($wrongtime))
            return 'success';
        else
            return 'fail';
    }
}
