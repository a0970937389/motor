<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WrongTime extends Model
{
    //
    protected $table = 'wrongtime';
    
    protected $fillable = [
        'user_id', 'qid', 'wrong'
    ];
}
