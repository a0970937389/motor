<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class History extends Model
{
    //
    protected $table = 'history';
    
    protected $fillable = [
        'user_id', 'body', 'roadsc', 'road1', 'road2', 'qsc', 'q1', 'q2', 'q3', 'q4', 'q5'
    ];
}
