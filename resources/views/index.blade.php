<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Motorcycle_Test</title>
    <link rel="icon" href="img/icon.ico" type="image/x-icon" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"
        integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
    <link rel="stylesheet" type="text/css" href="css/style.css" />
    <link rel="stylesheet" type="text/css" href="css/loading.css" />
</head>

<body>
    <div id="dive-app" class="container">
        <div class="loading" title="點擊後(全螢幕)並開始"></div>
		<iframe src="https://dive.nutn.edu.tw/Experiment//kaleTestExperiment5.jsp?eid=11137&record=false" name="dive11137" 
			class="dive" frameborder="0"></iframe>
    </div>
	<script src="https://dive.nutn.edu.tw/Experiment/js/dive.linker.min.js"></script>
    <script src="js/linker_gu.js"></script>
    <script>
        var name = '{{ $user->name}}';
        function nameadd(){
            var input = drOSCE.diveLinker.getInputList();
            input = Object.values(input);
            for(let i=0; i<input.length; i++){
                if(input[i].name == "username"){
                    let namein = input[i].id;
                    drOSCE.diveLinker.setInput(namein, name);
                }
            }
        }
        function setquiz(){
            var quizarr = new Array();
            var rand, now, choosen = [0, 0, 0, 0, 0];
            while(quizarr.length <= 4){
                rand = Math.floor(Math.random() * 11);
                if(quizarr.includes(rand) == false && rand != 0){
                    quizarr.push(rand);
                }
            }
            console.log(quizarr);
            var qnow = drOSCE.diveLinker.getInputList();
            qnow = Object.values(qnow);
            for(let i=0; i<qnow.length; i++){
                if(qnow[i].name == "nowindex"){
                    var nowindex = qnow[i].id;
                }
                if(qnow[i].name == "nowq"){
                    var nowq = qnow[i].id;
                }
                if(qnow[i].name == "nowchoose"){
                    var nowchoose = qnow[i].id;
                }
                if(qnow[i].name == "lastchoose"){
                    var lastchoose = qnow[i].id;
                }
            }
            console.log(nowindex);
            qqq = setInterval(function(){
                var nowin = drOSCE.diveLinker.getAttr(nowindex);
                var nowco = drOSCE.diveLinker.getAttr(nowchoose);
                var last = drOSCE.diveLinker.getAttr(lastchoose);
                nowin = parseInt(nowin);
                nowco = parseInt(nowco);
                last = parseInt(last);
                if(now != nowin){
                    drOSCE.diveLinker.setInput(nowq, quizarr[nowin]);
                    if(choosen[nowin] != 0){
                        drOSCE.diveLinker.setInput(nowchoose, choosen[nowin]);
                    }
                    if(last != 0 && choosen[now] != last){
                        choosen[now] = last;
                    }
                    now = nowin;
                }
            }, 100);
        }
        function test(){
            if(window.DeviceOrientationEvent) {
                window.addEventListener('deviceorientation', function(event) {
                    var alpha = event.alpha,
                        beta = event.beta,
                        gamma = event.gamma;
                        
                    console.log(alpha);
                    console.log(beta);
                    console.log(gamma);
              
                }, false);
            }else{
               console.log('error');
            }   
        }
    </script>
</body>

</html>